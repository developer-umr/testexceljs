# EjemploExceljs
#installation
npm install --save exceljs@^1.5.1
npm install --save file-saver@^1.3.8
#example
https://www.ngdevelop.tech/export-to-excel-in-angular-6/
#config tsconfig.js
"compilerOptions": {
    ...
    "paths": {
      "exceljs": [
        "node_modules/exceljs/dist/exceljs.min"
      ]
    }
  }